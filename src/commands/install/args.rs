use clap::{Args, Subcommand};

#[derive(Debug, Args)]
pub struct InstallCommands {
    #[clap(subcommand)]
    pub package: InstallPackages,
}

#[derive(Debug, Subcommand)]
pub enum InstallPackages {
    /// Full (NGINX, Docker, nvm)
    Full,

    /// NGINX
    Nginx,

    /// Docker
    Docker,

    /// nvm
    Nvm,

    /// Wordpress (NGINX, PHP-FPM, MariaDB)
    WordPress,
}
