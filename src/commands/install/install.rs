use clap::Parser;

#[derive(Parser, Debug)]
pub enum Install {
    Nginx,
    Docker,
    Nvm,
}

impl Install {
    pub fn install(&self) {
        match self {
            Install::Nginx => {
                println!("Installing NGINX...")
            }
            Install::Docker => {
                println!("Installing Docker...")
            }
            Install::Nvm => {
                println!("Installing NVM...")
            }
        }
    }
}
