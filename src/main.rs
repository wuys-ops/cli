mod args;
mod commands;
use nix::unistd::Uid;
use std::process;

use clap::Parser;

use args::OpsArgs;

fn main() {
    if !Uid::effective().is_root() {
        println!("wosp require root previleges to run. Exiting...");
        process::exit(1);
    }
    let args = OpsArgs::parse();

    println!("{:?}", args)
}
