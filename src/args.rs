use clap::{Parser, Subcommand};

use crate::commands::install::args::InstallCommands;

/// Simple tool to deploy webapp(s)
#[derive(Debug, Parser)]
#[command(author="wuys", version, about, long_about = None)]
pub struct OpsArgs {
    #[clap(subcommand)]
    pub command: OpsCommands,
}

#[derive(Debug, Subcommand)]
pub enum OpsCommands {
    /// Install package(s)
    #[command(visible_alias = "i")]
    Install(InstallCommands),

    /// Configure package(s)
    #[command(visible_alias = "c")]
    Config,

    /// Deploy webapp(s)
    #[command(visible_alias = "d")]
    Deploy,

    /// Backup database(s)
    #[command(visible_alias = "b")]
    Backup,

    /// Restore database(s)
    #[command(visible_alias = "r")]
    Restore,

    /// Update package(s)
    #[command(visible_alias = "u")]
    Update,

    /// Upgrade system
    #[command(visible_alias = "U")]
    Upgrade,

    /// SSL
    #[command(visible_alias = "s")]
    Ssl,
}
